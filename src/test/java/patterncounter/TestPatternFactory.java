package patterncounter;

import org.junit.Test;
import patterncounter.patterns.NumberPattern;
import patterncounter.patterns.PhrasePattern;
import patterncounter.patterns.WordPattern;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

public class TestPatternFactory {



  @Test
  public void testCreateWordFactory() {
    Pattern p = PatternFactory.createPattern(PatternFactory.PatternType.findByName("word"));
    assertThat(p, instanceOf(WordPattern.class));
  }

  @Test
  public void testCreateNumberFactory() {
    Pattern p = PatternFactory.createPattern(PatternFactory.PatternType.findByName("number"));
    assertThat(p, instanceOf(NumberPattern.class));
  }

  @Test
  public void testCreatePhraseFactory() {
    Pattern p = PatternFactory.createPattern(PatternFactory.PatternType.findByName("phrase"));
    assertThat(p, instanceOf(PhrasePattern.class));
  }
}
