package patterncounter.patterns;


import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;
import patterncounter.InputReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class TestWordPattern {

  private WordPattern objectToTest = new WordPattern();

  @Test
  public void testExecute() {

    String fileName = "input.txt";
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(fileName).getFile());

    InputReader reader = null;
    try {
      reader = InputReader.build(file.getAbsolutePath());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    Map<String, Long> countedTokens = null;
    countedTokens = objectToTest.execute(reader);
    assertThat(countedTokens.size(), is(7));
    assertThat(countedTokens, IsMapContaining.hasEntry("quick", new Long(4)));

  }

}
