package patterncounter;


import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

public class TestOutputWriter {




  private OutputWriter objectToTest;

  @Test
  public void testRetrieveTokens() {

    String fileName = "output.txt";

    HashMap<String, Long> results = new HashMap<>();
    results.put("the", 2L);
    results.put("quick", 2L);
    results.put("brown", 1L);

    objectToTest = OutputWriter.build(fileName, results);
    try {
      objectToTest.writeResults();
    } catch ( IOException e ){
      fail();
    }

  }


}
