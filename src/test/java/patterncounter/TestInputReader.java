package patterncounter;


import org.junit.Test;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class TestInputReader {




  private InputReader objectToTest;

  @Test
  public void testRetrieveTokens() {

    String fileName = "input.txt";
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(fileName).getFile());

    InputReader reader = null;
    try {
      reader = InputReader.build(file.getAbsolutePath());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    List<String> list = null;
    list = reader.retrieveTokens();
    assertThat(list.size(), is(19));

  }


}
