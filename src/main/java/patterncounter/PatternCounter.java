package patterncounter;

import java.io.FileNotFoundException;
import java.io.IOException;

public class PatternCounter {

  public static final String OUTPUT_FILE = "output.txt";
  public static final String USAGE = "Usage: java patterncounter.PatternCounter -cp pattern-counter.jar " +
          "/tmp/input.txt {word|number|phrase}\n";


  public static void main(String[] args) {

    //Check for mandatory arguments
    if (args.length < 2) {
      System.out.println(USAGE);
    }

    Pattern p = null;
    InputReader ir = null;

    //Create the input file reader
    try {
      ir = InputReader.build(args[0]);
    } catch (FileNotFoundException e) {
      System.out.println("Input file not found: " + args[0] + "\n " + USAGE);
      System.exit(1);
    }

    //Create the pattern handler impl
    try {
      p = PatternFactory.createPattern(PatternFactory.PatternType.findByName(args[1]));
    } catch (RuntimeException re) {
      System.out.println("Pattern not found: " + args[1] + "\n  " + USAGE);
      System.exit(1);
    }


    //Create the output file writer with the pattern results and write.
    OutputWriter ow = null;
    ow = OutputWriter.build(OUTPUT_FILE, p.execute(ir));
    try {
      ow.writeResults();
    } catch (IOException e) {
      System.out.println("Unable to write to output file, check dir/file permissions");
      System.exit(1);
    }
  }

}
