package patterncounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputReader {

  private Scanner scanner;

  public static InputReader build(String fileName) throws FileNotFoundException {
    InputReader t = new InputReader();
    t.scanner = createScanner(fileName);
    return t;
  }

  public List<String> retrieveTokens() {
    List<String> tokens = new ArrayList<>();

    while (scanner.hasNext() ) {
      tokens.add(scanner.next());
    }

    return tokens;
  }

  private static Scanner createScanner(String fileName) throws FileNotFoundException {
    return new Scanner(new File(fileName));
  }

}
