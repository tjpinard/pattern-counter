package patterncounter.patterns;

import patterncounter.InputReader;
import patterncounter.Pattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PhrasePattern implements Pattern {

    @Override
    public Map<String, Long> execute(InputReader reader) {
        List<String> tokens;
        tokens = reader.retrieveTokens();
        List<String> phrases = buildPhrases(tokens, 0);
        return phrases.stream()
                .collect(Collectors.groupingBy(Function.identity(),
                        Collectors.counting()));
    }


    private List<String> buildPhrases(List<String> tokens, int cursor) {
        ArrayList<String> phrases = new ArrayList<>();
        phrases.add(tokens.get(cursor) + " " + tokens.get(cursor + 1) + " " + tokens.get(cursor + 2));

        if (tokens.size() > cursor + 3) {
            phrases.addAll(buildPhrases(tokens, cursor + 1));
        }

        return phrases;
    }
}
