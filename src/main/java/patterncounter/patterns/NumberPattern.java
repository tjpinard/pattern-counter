package patterncounter.patterns;

import patterncounter.InputReader;
import patterncounter.Pattern;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class NumberPattern implements Pattern {

  @Override
  public Map<String, Long> execute(InputReader reader) {
    List<String> tokens = null;
    tokens = reader.retrieveTokens();
    return tokens.stream()
      .filter(x -> Pattern.isNumber(x))
      .collect(Collectors.groupingBy(Function.identity(),
        Collectors.counting()));
  }
}
