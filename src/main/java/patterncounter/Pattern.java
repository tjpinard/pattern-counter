package patterncounter;

import java.util.Map;

public interface Pattern {

  Map<String, Long> execute(InputReader reader) ;

  static boolean isNumber(String str) {
    try {
      double number = Double.parseDouble(str);
    } catch(NumberFormatException e) {
      return false;
    }
    return true;
  }
}
