package patterncounter;

import patterncounter.patterns.NumberPattern;
import patterncounter.patterns.PhrasePattern;
import patterncounter.patterns.WordPattern;

import java.util.HashMap;
import java.util.Map;

public class PatternFactory {


  public static Pattern createPattern(PatternType type) {

    switch (type) {
      case WORD:
        return new WordPattern();
      case NUMBER:
        return new NumberPattern();
      case PHRASE:
        return new PhrasePattern();
    }

    throw new RuntimeException("Pattern Not Found");
  }


  public enum PatternType {
    WORD("word"),
    NUMBER("number"),
    PHRASE("phrase");

    String name;
    private static final Map<String, PatternType> names = new HashMap<>();

    PatternType(String name) {
      this.name = name;
    }

    static {
      for (PatternType p : PatternType.values()) {
        names.put(p.name, p);
      }
    }

    public static PatternType findByName(String name) {
      return names.get(name);
    }


  }


}
