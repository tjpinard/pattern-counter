package patterncounter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class OutputWriter {

  private String fileName;
  private Map<String, Long> results;

  public static OutputWriter build(String fileName, Map<String, Long> results) {
    OutputWriter t = new OutputWriter();

    t.fileName = fileName;
    t.results = results;
    return t;
  }

  public void writeResults() throws IOException {
    FileWriter fileWriter = new FileWriter(fileName);
    PrintWriter printWriter = new PrintWriter(fileWriter);
    for (Map.Entry<String, Long> entry : results.entrySet()) {
      printWriter.printf("%s, %d\n", entry.getKey(), entry.getValue());
    }
    printWriter.close();

  }


}
