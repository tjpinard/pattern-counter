# pattern-counter

command line util that collects counts given pattern. 

## Getting Started

./gradlew clean build


java -cp ./build/libs/pattern-counter-1.0-SNAPSHOT.jar
 pattern-counter/PatternCounter /tmp/input.txt {word|number|phrase}
